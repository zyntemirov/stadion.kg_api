const morgan = require('morgan');
const express = require('express');
const bodyParser = require('body-parser');

var app = express();

const filterRoutes = require('./api/routes/filterStadiums');
const stadiums = require('./api/routes/stadiums');
const books = require('./api/routes/books');
const users = require('./api/routes/users');

app.use(morgan('dev'));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/filter' , filterRoutes);
app.use('/stadiums' , stadiums);
app.use('/books' , books);
app.use('/users' , users);

app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;