const express = require('express');
const router = express.Router();
const cors = require('cors')({origin: true});
const admin = require('firebase-admin');

const booksRef = admin.database().ref('books');

router.get('/detail/:stadium_id', (req, res) => {
    let id = req.params.stadium_id;

    return booksRef.
    orderByChild('stadium_id').equalTo(id).on('value', function(snapshot) {
        res.status(200).json(snapshot.val());
    });
});

router.get('/user/:uid', (req, res) => {
    let uid = req.params.uid;

    return booksRef.
    orderByChild('receiver').equalTo(uid).on('value', function(snapshot) {
        res.status(200).json(snapshot.val());
    });
});


router.get('/detail/:uid/:st_id/:status', (req, res) => {
    let uid = req.params.uid;
    let stadium_id = req.params.st_id;
    let status = req.params.status;
    let query = status+uid+stadium_id;
    console.log(query);
    return booksRef.
    orderByChild('status_stadium_id').equalTo(query).on('value', function(snapshot) {
        res.status(200).json(snapshot.val());
    });
});


module.exports = router;