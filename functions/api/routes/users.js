const express = require('express');
const router = express.Router();
const cors = require('cors')({origin: true});
const admin = require('firebase-admin');

const usersRef = admin.database().ref('users');

router.get('/create/token/:userId', (req, res) => {
    let uid = req.params.userId;

    return admin.auth().createCustomToken(uid)
        .then(function(customToken) {
            res.status(200).json(customToken);
        })
        .catch(function(error) {
            console.log('Error creating custom token:', error);
            res.status(200).json(null);
        });

});


router.get('/check/:token', (req, res) => {
    let idToken = req.params.token;
    // idToken comes from the client app
    return admin.auth().verifyIdToken(idToken, true)
        .then(function(decodedToken) {
            let uid = decodedToken.uid;
            res.status(200).json(uid);
        }).catch(function(error) {
            console.log('Error creating custom token:', error);
            res.status(200).json(null);
        });
});

router.get('/detail/:number', (req, res) => {
    let phoneNumber = req.params.number;
    return usersRef.orderByChild('phone_number').once('value', function(snapshot) {
        snapshot.forEach(function(item) {
            let phoneNums = item.val().phone_number;
            console.log(item.val().phone_number)
            if (phoneNums.includes(phoneNumber)){
                res.status(200).json(item.val());
            }
        });
        res.status(200).json(null)
    });
});

module.exports = router;