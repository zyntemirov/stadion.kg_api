const express = require('express');
const router = express.Router();
const cors = require('cors')({origin: true});
const admin = require('firebase-admin');

const stadiumsRef = admin.database().ref('stadiums');

router.get('/', (req, res) => {
    return stadiumsRef.on('value', function(snapshot) {
        res.status(200).json(snapshot.val());
    });
});

router.get('/:limit', (req, res) => {
    let limit = parseInt(req.params.limit);

    return stadiumsRef.
        orderByKey().limitToFirst(parseInt(limit)).on('value', function(snapshot) {
        res.status(200).json(snapshot.val());
    });
});

router.get('/detail/:id', (req, res) => {
    let id = req.params.id;

    return stadiumsRef.
    orderByChild('stadiumId').equalTo(id).on('value', function(snapshot) {
        res.status(200).json(snapshot.val());
    });
});

router.get('/user/:user_id', (req, res) => {
    let user_id = req.params.user_id;

    return stadiumsRef.
    orderByChild('userId').equalTo(user_id).on('value', function(snapshot) {
        res.status(200).json(snapshot.val());
    });
});

router.get('/category/:type', (req, res) => {
    let type = req.params.type;

    if (type === 'Bolshie') {
        type = 'Большой';
    } else if (type === 'Mini') {
        type = 'Мини';
    } else type = 'Футзал';

    return stadiumsRef.
    orderByChild('type').equalTo(type).on('value', function(snapshot) {
        res.status(200).json(snapshot.val());
    });
});

module.exports = router;