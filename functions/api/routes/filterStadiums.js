const express = require('express');
const router = express.Router();
const cors = require('cors')({origin: true});
const admin = require('firebase-admin');

const stadiumsRef = admin.database().ref('stadiums');
const booksRef = admin.database().ref('books');

router.get(
    ('/:type/:lowPrice/:highPrice/:ceiling/:infrastructure/:date/:startTime/:endTime'),
    (req, res) => {
      let lowPrice = parseInt(req.params.lowPrice);
      let highPrice = parseInt(req.params.highPrice);
      let type = req.params.type;

      let date = req.params.date;
      let startTime = req.params.startTime;
      let endTime = req.params.endTime;

      let string_ceiling = req.params.ceiling;
      let ceiling = string_ceiling == 'true';
      let infrastructure = req.params.infrastructure;

      if (type === 'Bolshie') {
        type = 'Большой';
      } else if (type === 'Mini') {
        type = 'Мини';
      } else if (type === 'Futzal') {
        type = 'Футзал';
      } else if (type === 'BolshieMini'){
        type = 'БольшойМини';
      } else if (type === 'BolshieFutzal'){
        type = 'БольшойФутзал';
      } else if (type === 'MiniFutzal'){
        type = 'МиниФутзал';
      } else if (type === 'BolshieMiniFutzal'){
        type = 'all';
      }

      if (string_ceiling === 'all') {
        ceiling = string_ceiling;
      }

      if (date === 'all' || startTime === 'all' || endTime === 'all') {
        startTime = 'all';
        endTime = 'all';
        date = 'all'
      }

      StadiumIdsOnBook();

      function StadiumIdsOnBook() {
        let stadiumIdonBooks = '';
        let stadiumIds = [];
        var itemsProcessed = 0;
        var count = 0;

        if (date !== 'all' && startTime !== 'all' && endTime !== 'all') {
          booksRef.orderByChild('date').
              equalTo(date).
              on('value', (snapshot) => {

                snapshot.forEach((book) => {
                  count++;
                });

                if (count === 0) {
                  callback(stadiumIdonBooks);
                }

                snapshot.forEach((book, index, array) => {
                  if ( (book.val().start_time >= startTime && book.val().end_time <= endTime) || ( book.val().start_time > startTime && book.val().start_time < endTime) || ( book.val().end_time > startTime && book.val().end_time < endTime)) {
                    if (!stadiumIdonBooks.includes(book.val().stadium_id))
                      stadiumIdonBooks += book.val().stadium_id;
                  }

                  itemsProcessed++;

                  if (itemsProcessed === count) {
                    callback(stadiumIdonBooks);
                  }

                });
              });
        }
        else {
            callback(stadiumIdonBooks);
        }
      }

      function callback(stadiumIdonBooks) {
        console.log(stadiumIdonBooks +' stadiumIdOnBook');

        if (stadiumIdonBooks === '') {
          return cors(req, res, () => {
            if (req.method !== 'GET') {
              return res.status(404).json({
                message: 'Not allowed',
              });
            }
            let stadiums = [];

            return stadiumsRef.orderByChild('price').
                startAt(lowPrice).
                endAt(highPrice).
                on('value', (snapshot) => {
                  snapshot.forEach((stadium) => {
                    if (type === 'all') {
                      if (ceiling === 'all') {
                        if (infrastructure === 'all') {
                          stadiums.push({
                            id: stadium.key,
                            address: stadium.val().address,
                            imgNames: stadium.val().imgNames,
                            imgURL: stadium.val().imgURL,
                            ceiling: stadium.child('infrastructure').
                                val().ceiling,
                            dressing_room: stadium.child('infrastructure').
                                val().dressing_room,
                            lighting: stadium.child('infrastructure').
                                val().lighting,
                            shower: stadium.child('infrastructure').
                                val().shower,
                            tribune: stadium.child('infrastructure').
                                val().tribune,
                            latitude: stadium.val().latitude,
                            longitude: stadium.val().longitude,
                            name: stadium.val().name,
                            price: stadium.val().price,
                            stadiumId: stadium.val().stadiumId,
                            type: stadium.val().type,
                            userId: stadium.val().userId,
                          });
                        } else {
                          if (stadium.val().
                              filterByInfrastructure.
                              includes(infrastructure)) {
                            stadiums.push({
                              id: stadium.key,
                              address: stadium.val().address,
                              imgNames: stadium.val().imgNames,
                              imgURL: stadium.val().imgURL,
                              ceiling: stadium.child('infrastructure').
                                  val().ceiling,
                              dressing_room: stadium.child('infrastructure').
                                  val().dressing_room,
                              lighting: stadium.child('infrastructure').
                                  val().lighting,
                              shower: stadium.child('infrastructure').
                                  val().shower,
                              tribune: stadium.child('infrastructure').
                                  val().tribune,
                              latitude: stadium.val().latitude,
                              longitude: stadium.val().longitude,
                              name: stadium.val().name,
                              price: stadium.val().price,
                              stadiumId: stadium.val().stadiumId,
                              type: stadium.val().type,
                              userId: stadium.val().userId,
                            });
                          }
                        }
                      } else {
                        if (stadium.child('infrastructure').
                            child('ceiling').
                            val() === ceiling) {
                          if (infrastructure === 'all') {
                            stadiums.push({
                              id: stadium.key,
                              address: stadium.val().address,
                              imgNames: stadium.val().imgNames,
                              imgURL: stadium.val().imgURL,
                              ceiling: stadium.child('infrastructure').
                                  val().ceiling,
                              dressing_room: stadium.child('infrastructure').
                                  val().dressing_room,
                              lighting: stadium.child('infrastructure').
                                  val().lighting,
                              shower: stadium.child('infrastructure').
                                  val().shower,
                              tribune: stadium.child('infrastructure').
                                  val().tribune,
                              latitude: stadium.val().latitude,
                              longitude: stadium.val().longitude,
                              name: stadium.val().name,
                              price: stadium.val().price,
                              stadiumId: stadium.val().stadiumId,
                              type: stadium.val().type,
                              userId: stadium.val().userId,
                            });
                          } else {
                            if (stadium.val().
                                filterByInfrastructure.
                                includes(infrastructure)) {
                              stadiums.push({
                                id: stadium.key,
                                address: stadium.val().address,
                                imgNames: stadium.val().imgNames,
                                imgURL: stadium.val().imgURL,
                                ceiling: stadium.child('infrastructure').
                                    val().ceiling,
                                dressing_room: stadium.child('infrastructure').
                                    val().dressing_room,
                                lighting: stadium.child('infrastructure').
                                    val().lighting,
                                shower: stadium.child('infrastructure').
                                    val().shower,
                                tribune: stadium.child('infrastructure').
                                    val().tribune,
                                latitude: stadium.val().latitude,
                                longitude: stadium.val().longitude,
                                name: stadium.val().name,
                                price: stadium.val().price,
                                stadiumId: stadium.val().stadiumId,
                                type: stadium.val().type,
                                userId: stadium.val().userId,
                              });
                            }
                          }
                        }
                      }
                    } else {
                      if (type.includes(stadium.val().type )) {
                        if (ceiling === 'all') {
                          if (infrastructure === 'all') {
                            stadiums.push({
                              id: stadium.key,
                              address: stadium.val().address,
                              imgNames: stadium.val().imgNames,
                              imgURL: stadium.val().imgURL,
                              ceiling: stadium.child('infrastructure').
                                  val().ceiling,
                              dressing_room: stadium.child('infrastructure').
                                  val().dressing_room,
                              lighting: stadium.child('infrastructure').
                                  val().lighting,
                              shower: stadium.child('infrastructure').
                                  val().shower,
                              tribune: stadium.child('infrastructure').
                                  val().tribune,
                              latitude: stadium.val().latitude,
                              longitude: stadium.val().longitude,
                              name: stadium.val().name,
                              price: stadium.val().price,
                              stadiumId: stadium.val().stadiumId,
                              type: stadium.val().type,
                              userId: stadium.val().userId,
                            });
                          } else {
                            if (stadium.val().
                                filterByInfrastructure.
                                includes(infrastructure)) {
                              stadiums.push({
                                id: stadium.key,
                                address: stadium.val().address,
                                imgNames: stadium.val().imgNames,
                                imgURL: stadium.val().imgURL,
                                ceiling: stadium.child('infrastructure').
                                    val().ceiling,
                                dressing_room: stadium.child('infrastructure').
                                    val().dressing_room,
                                lighting: stadium.child('infrastructure').
                                    val().lighting,
                                shower: stadium.child('infrastructure').
                                    val().shower,
                                tribune: stadium.child('infrastructure').
                                    val().tribune,
                                latitude: stadium.val().latitude,
                                longitude: stadium.val().longitude,
                                name: stadium.val().name,
                                price: stadium.val().price,
                                stadiumId: stadium.val().stadiumId,
                                type: stadium.val().type,
                                userId: stadium.val().userId,
                              });
                            }
                          }
                        } else {
                          if (stadium.child('infrastructure').
                              child('ceiling').
                              val() === ceiling) {
                            if (infrastructure === 'all') {
                              stadiums.push({
                                id: stadium.key,
                                address: stadium.val().address,
                                imgNames: stadium.val().imgNames,
                                imgURL: stadium.val().imgURL,
                                ceiling: stadium.child('infrastructure').
                                    val().ceiling,
                                dressing_room: stadium.child('infrastructure').
                                    val().dressing_room,
                                lighting: stadium.child('infrastructure').
                                    val().lighting,
                                shower: stadium.child('infrastructure').
                                    val().shower,
                                tribune: stadium.child('infrastructure').
                                    val().tribune,
                                latitude: stadium.val().latitude,
                                longitude: stadium.val().longitude,
                                name: stadium.val().name,
                                price: stadium.val().price,
                                stadiumId: stadium.val().stadiumId,
                                type: stadium.val().type,
                                userId: stadium.val().userId,
                              });
                            } else {
                              if (stadium.val().
                                  filterByInfrastructure.
                                  includes(infrastructure)) {
                                stadiums.push({
                                  id: stadium.key,
                                  address: stadium.val().address,
                                  imgNames: stadium.val().imgNames,
                                  imgURL: stadium.val().imgURL,
                                  ceiling: stadium.child('infrastructure').
                                      val().ceiling,
                                  dressing_room: stadium.child(
                                      'infrastructure').
                                      val().dressing_room,
                                  lighting: stadium.child('infrastructure').
                                      val().lighting,
                                  shower: stadium.child('infrastructure').
                                      val().shower,
                                  tribune: stadium.child('infrastructure').
                                      val().tribune,
                                  latitude: stadium.val().latitude,
                                  longitude: stadium.val().longitude,
                                  name: stadium.val().name,
                                  price: stadium.val().price,
                                  stadiumId: stadium.val().stadiumId,
                                  type: stadium.val().type,
                                  userId: stadium.val().userId,
                                });
                              }
                            }
                          }
                        }
                      }
                    }

                  });
                  res.status(200).json(stadiums);
                }, (error) => {
                  res.status(error.code).json({
                    message: 'Something went wrong. ${error.message}',
                  });
                });
          });
        }
        else {
          return cors(req, res, () => {
            if (req.method !== 'GET') {
              return res.status(404).json({
                message: 'Not allowed',
              });
            }
            let stadiums = [];

            return stadiumsRef.orderByChild('price').
                startAt(lowPrice).
                endAt(highPrice).
                on('value', (snapshot) => {
                  snapshot.forEach((stadium) => {
                    if (type === 'all') {
                      if (ceiling === 'all') {
                        if (infrastructure === 'all') {
                          if (!stadiumIdonBooks.includes(stadium.val().stadiumId))
                            stadiums.push({
                              id: stadium.key,
                              address: stadium.val().address,
                              imgNames: stadium.val().imgNames,
                              imgURL: stadium.val().imgURL,
                              ceiling: stadium.child('infrastructure').
                                  val().ceiling,
                              dressing_room: stadium.child('infrastructure').
                                  val().dressing_room,
                              lighting: stadium.child('infrastructure').
                                  val().lighting,
                              shower: stadium.child('infrastructure').
                                  val().shower,
                              tribune: stadium.child('infrastructure').
                                  val().tribune,
                              latitude: stadium.val().latitude,
                              longitude: stadium.val().longitude,
                              name: stadium.val().name,
                              price: stadium.val().price,
                              stadiumId: stadium.val().stadiumId,
                              type: stadium.val().type,
                              userId: stadium.val().userId,
                            });
                        } else {
                          if (stadium.val().filterByInfrastructure.includes(infrastructure)) {
                            if (!stadiumIdonBooks.includes(stadium.val().stadiumId))
                              stadiums.push({
                                id: stadium.key,
                                address: stadium.val().address,
                                imgNames: stadium.val().imgNames,
                                imgURL: stadium.val().imgURL,
                                ceiling: stadium.child('infrastructure').
                                    val().ceiling,
                                dressing_room: stadium.child('infrastructure').
                                    val().dressing_room,
                                lighting: stadium.child('infrastructure').
                                    val().lighting,
                                shower: stadium.child('infrastructure').
                                    val().shower,
                                tribune: stadium.child('infrastructure').
                                    val().tribune,
                                latitude: stadium.val().latitude,
                                longitude: stadium.val().longitude,
                                name: stadium.val().name,
                                price: stadium.val().price,
                                stadiumId: stadium.val().stadiumId,
                                type: stadium.val().type,
                                userId: stadium.val().userId,
                              });
                          }
                        }
                      } else {
                        if (stadium.child('infrastructure').
                            child('ceiling').
                            val() === ceiling) {
                          if (infrastructure === 'all') {
                            if (!stadiumIdonBooks.includes(stadium.val().stadiumId))
                              stadiums.push({
                                id: stadium.key,
                                address: stadium.val().address,
                                imgNames: stadium.val().imgNames,
                                imgURL: stadium.val().imgURL,
                                ceiling: stadium.child('infrastructure').
                                    val().ceiling,
                                dressing_room: stadium.child('infrastructure').
                                    val().dressing_room,
                                lighting: stadium.child('infrastructure').
                                    val().lighting,
                                shower: stadium.child('infrastructure').
                                    val().shower,
                                tribune: stadium.child('infrastructure').
                                    val().tribune,
                                latitude: stadium.val().latitude,
                                longitude: stadium.val().longitude,
                                name: stadium.val().name,
                                price: stadium.val().price,
                                stadiumId: stadium.val().stadiumId,
                                type: stadium.val().type,
                                userId: stadium.val().userId,
                              });
                          } else {
                            if (stadium.val().filterByInfrastructure.includes(infrastructure)) {
                              if (!stadiumIdonBooks.includes(stadium.val().stadiumId))
                                stadiums.push({
                                  id: stadium.key,
                                  address: stadium.val().address,
                                  imgNames: stadium.val().imgNames,
                                  imgURL: stadium.val().imgURL,
                                  ceiling: stadium.child('infrastructure').
                                      val().ceiling,
                                  dressing_room: stadium.child('infrastructure').
                                      val().dressing_room,
                                  lighting: stadium.child('infrastructure').
                                      val().lighting,
                                  shower: stadium.child('infrastructure').
                                      val().shower,
                                  tribune: stadium.child('infrastructure').
                                      val().tribune,
                                  latitude: stadium.val().latitude,
                                  longitude: stadium.val().longitude,
                                  name: stadium.val().name,
                                  price: stadium.val().price,
                                  stadiumId: stadium.val().stadiumId,
                                  type: stadium.val().type,
                                  userId: stadium.val().userId,
                                });
                            }
                          }
                        }
                      }
                    } else {
                      if (type.includes(stadium.val().type)) {
                        if (ceiling === 'all') {
                          if (infrastructure === 'all') {
                            if (!stadiumIdonBooks.includes(stadium.val().stadiumId))
                              stadiums.push({
                                id: stadium.key,
                                address: stadium.val().address,
                                imgNames: stadium.val().imgNames,
                                imgURL: stadium.val().imgURL,
                                ceiling: stadium.child('infrastructure').
                                    val().ceiling,
                                dressing_room: stadium.child('infrastructure').
                                    val().dressing_room,
                                lighting: stadium.child('infrastructure').
                                    val().lighting,
                                shower: stadium.child('infrastructure').
                                    val().shower,
                                tribune: stadium.child('infrastructure').
                                    val().tribune,
                                latitude: stadium.val().latitude,
                                longitude: stadium.val().longitude,
                                name: stadium.val().name,
                                price: stadium.val().price,
                                stadiumId: stadium.val().stadiumId,
                                type: stadium.val().type,
                                userId: stadium.val().userId,
                              });
                          } else {
                            if (stadium.val().filterByInfrastructure.includes(infrastructure)) {
                              if (!stadiumIdonBooks.includes(stadium.val().stadiumId))
                                stadiums.push({
                                  id: stadium.key,
                                  address: stadium.val().address,
                                  imgNames: stadium.val().imgNames,
                                  imgURL: stadium.val().imgURL,
                                  ceiling: stadium.child('infrastructure').
                                      val().ceiling,
                                  dressing_room: stadium.child('infrastructure').
                                      val().dressing_room,
                                  lighting: stadium.child('infrastructure').
                                      val().lighting,
                                  shower: stadium.child('infrastructure').
                                      val().shower,
                                  tribune: stadium.child('infrastructure').
                                      val().tribune,
                                  latitude: stadium.val().latitude,
                                  longitude: stadium.val().longitude,
                                  name: stadium.val().name,
                                  price: stadium.val().price,
                                  stadiumId: stadium.val().stadiumId,
                                  type: stadium.val().type,
                                  userId: stadium.val().userId,
                                });
                            }
                          }
                        } else {
                          if (stadium.child('infrastructure').
                              child('ceiling').
                              val() === ceiling) {
                            if (infrastructure === 'all') {
                              if (!stadiumIdonBooks.includes(stadium.val().stadiumId))
                                stadiums.push({
                                  id: stadium.key,
                                  address: stadium.val().address,
                                  imgNames: stadium.val().imgNames,
                                  imgURL: stadium.val().imgURL,
                                  ceiling: stadium.child('infrastructure').
                                      val().ceiling,
                                  dressing_room: stadium.child('infrastructure').
                                      val().dressing_room,
                                  lighting: stadium.child('infrastructure').
                                      val().lighting,
                                  shower: stadium.child('infrastructure').
                                      val().shower,
                                  tribune: stadium.child('infrastructure').
                                      val().tribune,
                                  latitude: stadium.val().latitude,
                                  longitude: stadium.val().longitude,
                                  name: stadium.val().name,
                                  price: stadium.val().price,
                                  stadiumId: stadium.val().stadiumId,
                                  type: stadium.val().type,
                                  userId: stadium.val().userId,
                                });
                            } else {
                              if (stadium.val().filterByInfrastructure.includes(infrastructure)) {
                                if (!stadiumIdonBooks.includes(stadium.val().stadiumId))
                                  stadiums.push({
                                    id: stadium.key,
                                    address: stadium.val().address,
                                    imgNames: stadium.val().imgNames,
                                    imgURL: stadium.val().imgURL,
                                    ceiling: stadium.child('infrastructure').
                                        val().ceiling,
                                    dressing_room: stadium.child(
                                        'infrastructure').
                                        val().dressing_room,
                                    lighting: stadium.child('infrastructure').
                                        val().lighting,
                                    shower: stadium.child('infrastructure').
                                        val().shower,
                                    tribune: stadium.child('infrastructure').
                                        val().tribune,
                                    latitude: stadium.val().latitude,
                                    longitude: stadium.val().longitude,
                                    name: stadium.val().name,
                                    price: stadium.val().price,
                                    stadiumId: stadium.val().stadiumId,
                                    type: stadium.val().type,
                                    userId: stadium.val().userId,
                                  });
                              }
                            }
                          }
                        }
                      }
                    }
                  });

                  res.status(200).json(stadiums);
                }, (error) => {
                  res.status(error.code).json({
                    message: 'Something went wrong. ${error.message}',
                  });
                });
          });
        }
      }

    });

router.get('/orderByOnAndroid', (req, res) => {
  return cors(req, res, () => {
    if (req.method !== 'GET') {
      return res.status(404).json({
        message: 'Not allowed',
      });
    }
    let stadiums = [];

    return stadiumsRef.orderByChild('price').
        limitToLast(10).
        on('value', (snapshot) => {
          snapshot.forEach((stadium) => {
            stadiums.push({
              id: stadium.key,
              address: stadium.val().address,
              imgURL: stadium.val().imgURL,
              latitude: stadium.val().latitude,
              longitude: stadium.val().longitude,
              name: stadium.val().name,
              price: stadium.val().price,
              stadiumId: stadium.val().stadiumId,
              type: stadium.val().type,
              userId: stadium.val().userId,
            });
          });

          res.status(200).json(stadiums);
        }, (error) => {
          res.status(error.code).json({
            message: 'Something went wrong. ${error.message}',
          });
        });
  });
});

module.exports = router;
