const functions = require('firebase-functions');
const {Storage} = require('@google-cloud/storage');
const spawn = require('child-process-promise').spawn;
const path = require('path');
const os = require('os');
const fs = require('fs');
const admin = require('firebase-admin');
const serviceAccount = require("./stadion-e9852-7098757247a9");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://stadion-e9852.firebaseio.com"
});

const projectId = 'stadion-e9852';

const storage = new Storage({
  projectId: projectId,
});

const app = require('./app');


//api for filter stadiums
exports.StadionFilter = functions.https.onRequest(app);

//send notification
exports.BookNotification = functions.database.ref('books/{bookId}')
    .onCreate((event) => {
      let book = event.val();

      const senderUid = book.userId;
      const receiverUid = book.receiver;
      let webToken = null;
      if (senderUid === receiverUid) {
          //if sender is receiver, don't send notification
          return;
      }

      const getInstanceIdPromise = admin.database().ref(`/users/${receiverUid}/tokens`).once('value');
      const getSenderUidPromise = admin.auth().getUser(senderUid);

      admin.database().ref(`/users/${receiverUid}/webToken`).once('value', (snapshot)=> {
            webToken = snapshot.val()
        },(errorObject) =>{
            console.log("The read failed: " + errorObject.code);
        });

      return Promise.all([getInstanceIdPromise , getSenderUidPromise]).then(results => {
          const instanceId = results[0].val();
          const sender = results[1];
          let title, icon;
          console.log('notifying ' + receiverUid + ' about ' + 'Azamat Zyntemirov' + ' from ' + senderUid);

          if (sender.displayName !== undefined){
              title = sender.displayName;
          }
          else title = sender.phoneNumber;

          if (sender.photoURL !== undefined){
              icon = sender.photoURL;
          }
          else icon = 'https://firebasestorage.googleapis.com/v0/b/stadion-e9852.appspot.com/o/icon_man.png?alt=media&token=81c6f9b6-a5f5-4e69-8a1b-8034ba605240';

          const payload = {
              notification: {
                  title: title,
                  body: 'запрос на аренду '+book.date+' c '+book.start_time+' до '+book.end_time,
                  icon: icon,
                  "color": "#000000",
                  "sound":"default"
              }
          };

          if (instanceId !== ''){
              instanceId.forEach(function(token) {
                  console.log(token);
                  admin.messaging().sendToDevice(token, payload)
                      .then(function (response) {
                          console.log("Successfully sent notification:", response);
                      })
                      .catch(function (error) {
                          console.log("Error sending book:", error);
                      });
              });
          }
          else if(webToken !== null){
              admin.messaging().sendToDevice(webToken, payload)
                  .then(function (response) {
                      console.log("Successfully sent web notification:", response);
                  }).then(()=>{

              })
              .catch(function (error) {
                  console.log("Error sending book:", error);
              });
          }
          else console.log("in database haven't got token")
      });
    });

//trigger for uploading new image and eventd quality
exports.ImageResize = functions.storage.object().onFinalize(object => {
  const bucket = object.bucket;
  const contentType = object.contentType;
  const filePath = object.name;
  const fileName = path.basename(filePath);
  console.log('File event detected, function execution started');

  // Exit if the image is already a thumbnail.
  if (fileName.startsWith('thumb_')) {
    console.log('Already a Thumbnail.');
    return null;
  }

  const destBucket = storage.bucket(bucket);
  const tmpFilePath = path.join(os.tmpdir(), fileName);
  const metadata = {contentType: contentType};

  return destBucket.file(filePath).download({
    destination: tmpFilePath,
  }).then(() => {
    console.log('Image downloaded locally to', tmpFilePath);
    // Generate a thumbnail using ImageMagick.
    return spawn('convert', [tmpFilePath, '-resize', 'x600', tmpFilePath]);
  }).then(() => {
    console.log('Thumbnail created at', tmpFilePath);
    // We add a 'thumb_' prefix to thumbnails file name. That's where we'll upload the thumbnail.
    const thumbFileName = `thumb_medium_${fileName}`;
    const thumbFilePath = path.join(path.dirname(filePath), thumbFileName);

    return destBucket.upload(tmpFilePath, {
      destination: thumbFilePath,
      metadata: metadata,
    });

  }).then(() => {
    return destBucket.file(filePath).download({
      destination: tmpFilePath,
    }).then(() => {
      console.log('Image downloaded locally to 2 image', tmpFilePath);
      // Generate a thumbnail using ImageMagick.
      return spawn('convert', [tmpFilePath, '-resize', 'x318', tmpFilePath]);
    }).then(() => {
      console.log('Thumbnail created at 2 image', tmpFilePath);
      // We add a 'thumb_' prefix to thumbnails file name. That's where we'll upload the thumbnail.
      const thumbFileName = `thumb_small_${fileName}`;
      const thumbFilePath = path.join(path.dirname(filePath), thumbFileName);

      return destBucket.upload(tmpFilePath, {
        destination: thumbFilePath,
        metadata: metadata,
      });
    });

  }).then(() => fs.unlinkSync(tmpFilePath));
});

//trigger for when deleted stadium on database automatically deleted image on storage
exports.deleteAllImageOfStadiumOnStorage = functions.database.ref(
    'stadiums/{stadiumId}').
    onDelete((event) => {
      let deletedValue = event.val().imgNames;
      let stadiumId = event.val().stadiumId;
      console.log(deletedValue);
      console.log(stadiumId);
      const bucket = storage.bucket('stadion-e9852.appspot.com');

      for (const item of deletedValue) {
        console.log(item);
        let filePath = `stadium/${stadiumId}/${item}`;
        let file = bucket.file(filePath);

        file.delete().then(() => {
          console.log(
              `Successfully deleted photo with UID: ${stadiumId}, image name : ${item}`);
        }).then(() => {
          let filePath = `stadium/${stadiumId}/thumb_medium_${item}`;
          let file = bucket.file(filePath);

          file.delete().then(() => {
            console.log(
                `Successfully deleted photo with UID: ${stadiumId}, medium image name : thumb_medium_${item}`);
          }).then(() => {
            let filePath = `stadium/${stadiumId}/thumb_small_${item}`;
            let file = bucket.file(filePath);

            file.delete().then(() => {
              console.log(
                  `Successfully deleted photo with UID: ${stadiumId}, small image name : thumb_small_${item}`);
            });
          });
        }).catch(err => {
          console.log(`Failed to remove photo, error: ${err}`);
        });
      }
    });

//trigger for when deleted stadium one image on database automatically deleted one image on storage
exports.deleteOneImageOfStadiumOnStorage = functions.database.ref(
    'stadiums/{stadiumId}/imgNames/{imgId}').
    onDelete((event, context) => {
      let deletedValue = event.val();
      let stadiumId = context.params.stadiumId;
      console.log(deletedValue);
      console.log(stadiumId);
      const bucket = storage.bucket('stadion-e9852.appspot.com');

      let filePath = `stadium/${stadiumId}/${deletedValue}`;
      let file = bucket.file(filePath);

      file.delete().then(() => {
        console.log(
            `Successfully deleted photo with UID: ${stadiumId}, image name : ${deletedValue}`);
      }).then(() => {
        let filePath = `stadium/${stadiumId}/thumb_medium_${deletedValue}`;
        let file = bucket.file(filePath);

        file.delete().then(() => {
          console.log(
              `Successfully deleted photo with UID: ${stadiumId}, medium image name : thumb_medium_${deletedValue}`);
        }).then(() => {
          let filePath = `stadium/${stadiumId}/thumb_small_${deletedValue}`;
          let file = bucket.file(filePath);

          file.delete().then(() => {
            console.log(
                `Successfully deleted photo with UID: ${stadiumId}, small image name : thumb_small_${deletedValue}`);
          });
        });
      }).catch(err => {
        console.log(`Failed to remove photo, error: ${err}`);
      });

    });


